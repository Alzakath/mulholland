"""Mulholland setup module"""

# To use a consistent encoding
from codecs import open
from os import path

# Always prefer setuptools over distutils
from setuptools import find_packages, setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='mulholland',
    version='0.0.1',
    description='Automatic Video Library Manager for TV Shows',
    long_description=long_description,

    # The project's main homepage.
    url='https://github.com/Alzakath/mulholland',

    # Author details
    author='Alzakath',
    author_email='herve.coatanhay@gmail.com',

    # Choose your license
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
    ],

    # What does your project relate to?
    keywords='video tv shows',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['gevent', 'pyzmq', 'webob'],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'mulholland=mulholland:main'
        ],
    },
)
