from webob import Request
from mulholland.conds import *


class TestConds:

    def test_len(self):
        assert (ALL(LEN(12), METHOD('POST'))(
            None,
            'POST',
            ['a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c', ],
            Request({})))

    def test_urls(self):
        assert not (URLS(['api', '*', 'v1'])(
            None,
            'GET',
            ['api', 'v1'],
            Request({})))
