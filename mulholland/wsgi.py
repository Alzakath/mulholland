import datetime
import logging
import uuid

import zmq.green as zmq
from gevent import pywsgi, wsgi

from webob import Request, Response, exc

from .conds import URLS
from .route import route, route_for

from .api import API
from contextlib import contextmanager


class RequestContext:

    def __init__(self, app, environ, request=None):
        self.app = app
        if request is None:
            request = Request(environ)
        self.request = request
        self.zsocket = None
        self.zpoller = None

    def create_dispatch_args(self):
        url = self.request.path_info.strip('/').split('/')
        return self.app, self.request.method, tuple(url), self

    @property
    def params(self):
        return dict(self.request.GET)

    @contextmanager
    def send(self):
        msg_in = pickle.dumps(self.request)
        self.zsocket.send_string(msg_in)
        sockets = dict(self.zpoller.poll())
        if request.zsocket in sockets:
            msg_out = request.zsocket.recv()
            yield msg_out
        else:
            yield ''

    def __enter__(self):
        self.zsocket = self.app.zcontext.socket(zmq.DEALER)
        self.zsocket.connect(self.app.socket)

        self.zpoller = zmq.Poller()
        self.zpoller.register(self.zsocket, zmq.POLLIN)
        self.zsocket.identity = uuid.uuid4().bytes
        return self

    def __exit__(self, exc_type, exc_value, tb):
        self.zpoller.unregister(self.zsocket)
        self.zsocket.close()
        self.zsocket = None
        self.zpoller = None


class WSGIApp:

    publisher = wsgi.WSGIServer

    def __init__(self, request_context=None,
                 socket='ipc:///tmp/mulholland_frontend'):
        if request_context is None:
            request_context = RequestContext
        self.request_context = request_context
        self.zcontext = zmq.Context()
        self.socket = socket

    def wsgi_app(self, environ, start_response):
        try:
            with self.request_context(self, environ) as request:
                response = self.handle_request(request)
        except exc.HTTPException as response:
            return response(environ, start_response)
        except Exception as e:
            logging.exception('')
            response = exc.HTTPInternalServerError()

        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def handle_request(self, request):

        args = request.create_dispatch_args()
        while isinstance(args, tuple):
            args = route(*args)(*args, **request.params)
        return args
        # now = datetime.datetime.now()
        # msg = 'request {}'.format(now)
        #
        # request.zsocket.send_string(msg)
        #
        # sockets = dict(request.zpoller.poll())
        #
        # if request.zsocket in sockets:
        #     msg = request.zsocket.recv()
        #     res = Response()
        #     res.text = msg.decode('ascii')
        #
        # return res

    def run(self, host='0.0.0.0', port=8000, **options):
        try:
            httpd = self.publisher((host, port), self, **options)
            httpd.serve_forever()
        except:
            self.zcontext.term()
            raise


@route_for(WSGIApp, '*', URLS(['api', 'v1', '*']))
def base_route(endpoint, method, url, request_context, **kwargs):
    print("API rule")
    return API(), method, url[2:], request_context
