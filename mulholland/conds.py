__all__ = ['URL', 'ALL', 'ANY', 'METHOD', 'INSTANCE', 'LEN', 'URLS']


def URL(position, value):
    def c(o, method, url, request):
        return url[position] == value
    return c


def ALL(*args):
    def c(o, method, url, request):
        return all(a(o, method, url, request) for a in args)
    return c


def ANY(*args):
    def c(o, method, url, request):
        return any(a(o, method, url, request) for a in args)
    return c


def METHOD(value):
    def c(o, method, url, request):
        return method == value
    return c


def INSTANCE(value):
    def c(o, method, url, request):
        return isinstance(o, value)
    return c


def LEN(value):
    def c(o, method, url, request):
        return len(url) == value
    return c


def URLS(urls):
    def c(o, method, url, request):
        return (
            len(url) >= len(urls) and
            all(
                urls[i] == url[i] for i in range(len(urls))
                if (urls[i] != '*' and not (urls[i].endswith('}') and
                                            urls[i].startswith('{')))
            )
        )
    return c
