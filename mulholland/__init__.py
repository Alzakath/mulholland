from multiprocessing import Process

from .wsgi import WSGIApp
from .zmq import Broker, Worker


def main():
    """Entry point for the application script"""
    app = WSGIApp()
    Process(target=app.run).start()

    broker = Broker()
    Process(target=broker.run).start()

    worker = Worker(0)
    Process(target=worker.run).start()
