import zmq.green as zmq


class Broker:
    def __init__(self, backend='ipc:///tmp/mulholland_backend',
                 frontend='ipc:///tmp/mulholland_frontend'):
        self.zcontext = None
        self.zfrontend = None
        self.zbackend = None
        self.backend = backend
        self.frontend = frontend

    def run(self):
        with self:
            zmq.device(zmq.QUEUE, self.zfrontend, self.zbackend)

    def __enter__(self):
        self.zcontext = zmq.Context()
        self.zfrontend = self.zcontext.socket(zmq.ROUTER)
        self.zfrontend.bind(self.frontend)

        self.zbackend = self.zcontext.socket(zmq.DEALER)
        self.zbackend.bind(self.backend)

        self.zpoller = zmq.Poller()
        self.zpoller.register(self.zfrontend, zmq.POLLIN)
        self.zpoller.register(self.zbackend, zmq.POLLIN)

        return self

    def __exit__(self, exc_type, exc_value, tb):
        self.zfrontend.close()
        self.zbackend.close()
        self.zcontext.term()
        self.zcontext = None
        self.zfrontend = None
        self.zbackend = None


class Worker:
    def __init__(self, id, socket='ipc:///tmp/mulholland_backend'):
        self.zcontext = None
        self.zsocket = None
        self.socket = socket
        self.id = id

    def run(self):
        with self:
            while True:
                ident, msg = self.zsocket.recv_multipart()
                print('Worker {}'.format(self.id))
                self.zsocket.send_multipart([ident, b'response'])

    def __enter__(self):
        self.zcontext = zmq.Context()
        self.zsocket = self.zcontext.socket(zmq.DEALER)
        self.zsocket.connect(self.socket)
        return self

    def __exit__(self, exc_type, exc_value, tb):
        self.zsocket.close()
        self.zcontext.term()
        self.zcontext = None
        self.zsocket = None
