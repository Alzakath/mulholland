from webob import exc

from .conds import ALL, ANY, INSTANCE, METHOD


class Router:

    def __init__(self):
        self.route_map = []

    def not_found(self, *args, **kwargs):
        raise exc.HTTPNotFound()

    def route(self, o, method, url, request):
        routes = [func for conds, func in self.route_map
                  if conds(o, method, url, request)]
        if len(routes) == 1:
            return routes[0]
        elif len(routes) > 1:
            raise LookupError('Ambiguous methods')
        else:
            return self.not_found

    def route_for(self, cls, method, *conds):
        conds = list(conds)
        if isinstance(method, list):
            conds.insert(0, ANY(*[METHOD(m) for m in method]))
        elif method != '*':
            conds.insert(0, METHOD(method))

        conds.insert(0, INSTANCE(cls))

        def wrapper(f):
            self.route_map.append((ALL(*conds), f))
            return f
        return wrapper

if not '_router' in locals():
    _router = Router()


def route_for(cls, method, *conds):
    return _router.route_for(cls, method, *conds)


def route(o, method, url, request):
    return _router.route(o, method, url, request)
