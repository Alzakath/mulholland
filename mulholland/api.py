from webob import Response

from .route import route, route_for


class API:
    pass


@route_for(API, '*')
def base_route(endpoint, method, url, request_context, **kwargs):
    return Response(url[0])
